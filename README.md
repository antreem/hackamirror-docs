# Hackamirror
Benvenuti ad hackamirror, il primo hackathon di [Antreem](http://antreem.com)!

Qui troverete un qualche indicazione che vi potrà essere utile durante questa nottata di sviluppo, ma prima di tutto le presentazioni e i requisiti.

# Presentazioni e Requisiti
Come sicuramente già saprete lo specchio funziona grazie ad un [Raspberry Pi](https://www.raspberrypi.org/) collegato allo schermo specchiato.

Nel Raspberry gira un processo [Node](https://nodejs.org/en/), è quindi necessario installarlo sul proprio sistema operativo, potete seguire come con [questa guida](https://nodejs.org/en/download/package-manager/).

Assieme a Node verrà installato anche il suo package manager `npm`, se mancasse è necessario installarlo a parte.

Sebbene `npm` sia sufficiente consiglio caldamente di installare anche [yarn](https://yarnpkg.com/lang/en/docs/install/).

Il processo Node di cui parlavo è [MagicMirror](https://github.com/MichMich/MagicMirror), una piattaforma dedicata agli smart mirror, che fa della modularità la sua forza.

Per installarlo sul vostro sistema clonate il repository

```
git clone https://github.com/MichMich/MagicMirror
```

Poi eseguite `yarn install` o `npm install` all'interno della cartella.

Una volta installati tutti i pacchetti richiesti è tempo di creare il file di configurazione, per ora possiamo limitarci a rinominare il file `config/config.js.sample` in `config/config.js`.

Infine potete lanciare il processo con `yarn start` o `npm start`, oppure lanciarlo come server usando `node serveronly`

Il server verrà lanciato su `http://localhost:8080`, potete aprire il vostro browser di fiducia e vederlo all'opera.

## tldr
- Installa node
- Installa npm (e yarn)
- `git clone https://github.com/MichMich/MagicMirror`
- `cd MagicMirror`
- `npm install` (o `yarn install`)
- `mv config/config.js.sample config/config.js`
- `node serveronly`
- Vai su [http://localhost:8080](http://localhost:8080)

# Il file di configurazione
`config/config.js` contiene la configurazione utilizzata da MagicMirror, è ben documentato e provvisto di commenti, l'array contenuto in `modules` prevede che i moduli vengano inseriti come oggetti JavaScript, ogni modulo deve indicare il nome (case sensitive), la posizione, ed eventualmente una configurazione da utilizzare.

```
{
    module: "Nome-Modulo",
    position: "top_left",
    config: {
        variable: "variabile",
    }
},
```