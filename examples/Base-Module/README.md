# Antreem Base Module for MagicMirror

## Configuration

Ensure you have node and npm (or yarn) installed along with gulp.

Change the package name to match the folder name (case sensitive), this ensures that MagicMirror instance can find the module.

Run `npm install` (or `yarn install`) to get the required modules and add any other module you need (`npm install module` or `yarn add module`).

Run `gulp scripts` to build the script in the module root directory, this _compiles_ the `index.js` script with all the modules required.

Run `gulp scss` to build the styles in the module root directory, this _compiles_ the `style.scss` file with all the partials required.

You can keep watching for changes using `gulp watch:scripts` or `gulp watch:scss`, also running the default task with `gulp` scripts and styles will be built, then gulp will watch for changes on both scripts and scss and compile them accordingly.

## Development

You can find more informations about the modules on the [MagicMirror documentation](https://github.com/MichMich/MagicMirror/blob/master/modules/README.md) or commented on the source code.
