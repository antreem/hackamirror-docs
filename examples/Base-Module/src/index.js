/* Magic Mirror
 * Module: Base-Module
 *
 * By Antreem
 */

// You can use Common.js syntax to require npm modules
//const anything = require("anything");

// Here we load the styling library we picked, emotion https://github.com/emotion-js/emotion
const css = require("emotion").css;

(() => {
    "use strict";
    // Using a self invoking function to ensure 'use strict'

    Module.register("Base-Module", {
        // Default module config.
        defaults: {
            //The configuration to use when the user doesn't select anything
            message: "Hello World",
            //It's a JavaScript object so you can add any type here
            array: [1, 2, 3],
            object: {
                int: 3,
                float: 3.14,
                string: "π",
                array: [3, 1, 4]
            },
            color: "#ff6600"
        },

        start: function() {
            Log.info("Starting module: " + this.name);
            this.sendSocketNotification("START");
        },

        socketNotificationReceived(notification, payload) {
            // This triggers only with notifications from the correspondent node_helper
        },

        notificationReceived(notification, payload, sender) {
            // This triggers with any notification received from any other module (this is **not** a socket notification)
        },

        getStyles() {
            // Here you can define an array of styles to load when the module boots up
            const styles = [
                `${this.name}.css`
                //, "index.css", "whatever.css"
            ];

            return styles;
        },

        getDom() {
            // Here you can define your module content, remember to return the node
            const wrapper = document.createElement("div");
            wrapper.innerText = this.config.message;

            // We can also define a style for our module
            // Here's some documentation for Emotion https://emotion.sh/docs/css
            const style = css`
                color: ${this.config.color};
            `;

            wrapper.classList.add(style);

            return wrapper;
        }
    });
})();
