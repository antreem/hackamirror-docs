/* Magic Mirror
 * Module: Base-Module
 *
 * By Antreem
 */

const NodeHelper = require("node_helper");

Module = {
    register: function(name, moduleDefinition) {
        console.log("Module config loaded: " + name);
        Module.configDefaults[name] = moduleDefinition.defaults;
    }
};

module.exports = NodeHelper.create({
    // We can also subclass the NodeHelper to add some functionality to our module

    start: function() {
        console.log("Starting node helper for: " + this.name);
    },

    socketNotificationReceived(notification, payload) {
        // This triggers only with notifications from the correspondent module
        if (notification === "START") {
            console.log(notification, "received");
        }
    }
});
