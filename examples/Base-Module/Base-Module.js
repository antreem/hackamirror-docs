(function(){function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s}return e})()({1:[function(require,module,exports){
(function (process){
'use strict';

function _interopDefault (ex) { return (ex && (typeof ex === 'object') && 'default' in ex) ? ex['default'] : ex; }

var emotionUtils = require('emotion-utils');
var stylisRuleSheet = _interopDefault(require('stylis-rule-sheet'));

var hyphenateRegex = /[A-Z]|^ms/g;
var processStyleName = emotionUtils.memoize(function (styleName) {
  return styleName.replace(hyphenateRegex, '-$&').toLowerCase();
});
var processStyleValue = function processStyleValue(key, value) {
  if (value == null || typeof value === 'boolean') {
    return '';
  }

  if (emotionUtils.unitless[key] !== 1 && key.charCodeAt(1) !== 45 && // custom properties
  !isNaN(value) && value !== 0) {
    return value + 'px';
  }

  return value;
};
var classnames = function classnames(args) {
  var len = args.length;
  var i = 0;
  var cls = '';

  for (; i < len; i++) {
    var arg = args[i];
    if (arg == null) continue;
    var toAdd = void 0;

    switch (typeof arg) {
      case 'boolean':
        break;

      case 'function':
        toAdd = classnames([arg()]);
        break;

      case 'object':
        {
          if (Array.isArray(arg)) {
            toAdd = classnames(arg);
          } else {
            toAdd = '';

            for (var k in arg) {
              if (arg[k] && k) {
                toAdd && (toAdd += ' ');
                toAdd += k;
              }
            }
          }

          break;
        }

      default:
        {
          toAdd = arg;
        }
    }

    if (toAdd) {
      cls && (cls += ' ');
      cls += toAdd;
    }
  }

  return cls;
};
var isBrowser = typeof document !== 'undefined';

/*

high performance StyleSheet for css-in-js systems

- uses multiple style tags behind the scenes for millions of rules
- uses `insertRule` for appending in production for *much* faster performance
- 'polyfills' on server side

// usage

import StyleSheet from 'glamor/lib/sheet'
let styleSheet = new StyleSheet()

styleSheet.inject()
- 'injects' the stylesheet into the page (or into memory if on server)

styleSheet.insert('#box { border: 1px solid red; }')
- appends a css rule into the stylesheet

styleSheet.flush()
- empties the stylesheet of all its contents

*/
// $FlowFixMe
function sheetForTag(tag) {
  if (tag.sheet) {
    // $FlowFixMe
    return tag.sheet;
  } // this weirdness brought to you by firefox


  for (var i = 0; i < document.styleSheets.length; i++) {
    if (document.styleSheets[i].ownerNode === tag) {
      // $FlowFixMe
      return document.styleSheets[i];
    }
  }
}

function makeStyleTag(opts) {
  var tag = document.createElement('style');
  tag.type = 'text/css';
  tag.setAttribute('data-emotion', opts.key || '');

  if (opts.nonce !== undefined) {
    tag.setAttribute('nonce', opts.nonce);
  }

  tag.appendChild(document.createTextNode('')) // $FlowFixMe
  ;
  (opts.container !== undefined ? opts.container : document.head).appendChild(tag);
  return tag;
}

function _StyleSheet(options) {
  this.isSpeedy = process.env.NODE_ENV === 'production'; // the big drawback here is that the css won't be editable in devtools

  this.tags = [];
  this.ctr = 0;
  this.opts = options;
}

function _inject() {
  if (this.injected) {
    throw new Error('already injected!');
  }

  this.tags[0] = makeStyleTag(this.opts);
  this.injected = true;
}

function _speedy(bool) {
  if (this.ctr !== 0) {
    // cannot change speedy mode after inserting any rule to sheet. Either call speedy(${bool}) earlier in your app, or call flush() before speedy(${bool})
    throw new Error("cannot change speedy now");
  }

  this.isSpeedy = !!bool;
}

function _insert(rule, sourceMap) {
  // this is the ultrafast version, works across browsers
  if (this.isSpeedy) {
    var tag = this.tags[this.tags.length - 1];
    var sheet = sheetForTag(tag);

    try {
      sheet.insertRule(rule, sheet.cssRules.length);
    } catch (e) {
      if (process.env.NODE_ENV !== 'production') {
        console.warn('illegal rule', rule); // eslint-disable-line no-console
      }
    }
  } else {
    var _tag = makeStyleTag(this.opts);

    this.tags.push(_tag);

    _tag.appendChild(document.createTextNode(rule + (sourceMap || '')));
  }

  this.ctr++;

  if (this.ctr % 65000 === 0) {
    this.tags.push(makeStyleTag(this.opts));
  }
}

function _ref(tag) {
  return tag.parentNode.removeChild(tag);
}

function _flush() {
  // $FlowFixMe
  this.tags.forEach(_ref);
  this.tags = [];
  this.ctr = 0; // todo - look for remnants in document.styleSheets

  this.injected = false;
}

var StyleSheet =
/*#__PURE__*/
function () {
  var _proto = _StyleSheet.prototype;
  _proto.inject = _inject;
  _proto.speedy = _speedy;
  _proto.insert = _insert;
  _proto.flush = _flush;
  return _StyleSheet;
}();

function createEmotion(context, options) {
  if (options === undefined) options = {};
  var key = options.key || 'css';

  if (process.env.NODE_ENV !== 'production') {
    if (/[^a-z-]/.test(key)) {
      throw new Error("Emotion key must only contain lower case alphabetical characters and - but \"" + key + "\" was passed");
    }
  } // $FlowFixMe


  var caches = context.__SECRET_EMOTION__;
  var current;

  function insertRule(rule) {
    current += rule;

    if (isBrowser) {
      sheet.insert(rule, currentSourceMap);
    }
  }

  var insertionPlugin = stylisRuleSheet(insertRule);

  if (caches === undefined) {
    var stylisOptions = {
      keyframe: false,
      global: false,
      prefix: options.prefix === undefined ? true : options.prefix,
      semicolon: true
    };

    if (process.env.NODE_ENV !== 'production') {
      stylisOptions.compress = false;
    }

    context.__SECRET_EMOTION__ = caches = {
      registered: {},
      inserted: {},
      sheet: new StyleSheet(options),
      stylis: new emotionUtils.Stylis(stylisOptions),
      nonce: options.nonce,
      key: key
    };
    caches.stylis.use(options.stylisPlugins)(insertionPlugin); // 🚀

    if (isBrowser) {
      caches.sheet.inject();
    }
  }

  var stylis = caches.stylis;
  var sheet = caches.sheet;
  var currentSourceMap = '';

  function handleInterpolation(interpolation, couldBeSelectorInterpolation) {
    if (interpolation == null) {
      return '';
    }

    switch (typeof interpolation) {
      case 'boolean':
        return '';

      case 'function':
        if (interpolation[emotionUtils.STYLES_KEY] !== undefined) {
          var selector = interpolation.toString();

          if (selector === 'NO_COMPONENT_SELECTOR' && process.env.NODE_ENV !== 'production') {
            throw new Error('Component selectors can only be used in conjunction with babel-plugin-emotion.');
          }

          return selector;
        }

        return handleInterpolation.call(this, this === undefined ? interpolation() : // $FlowFixMe
        interpolation(this.mergedProps, this.context), couldBeSelectorInterpolation);

      case 'object':
        return createStringFromObject.call(this, interpolation);

      default:
        var cached = caches.registered[interpolation];
        return couldBeSelectorInterpolation === false && cached !== undefined ? cached : interpolation;
    }
  }

  var objectToStringCache = new WeakMap();

  function createStringFromObject(obj) {
    if (objectToStringCache.has(obj)) {
      // $FlowFixMe
      return objectToStringCache.get(obj);
    }

    var string = '';

    function _ref(interpolation) {
      string += handleInterpolation.call(this, interpolation, false);
    }

    function _ref2(key) {
      if (typeof obj[key] !== 'object') {
        if (caches.registered[obj[key]] !== undefined) {
          string += key + "{" + caches.registered[obj[key]] + "}";
        } else {
          string += processStyleName(key) + ":" + processStyleValue(key, obj[key]) + ";";
        }
      } else {
        if (key === 'NO_COMPONENT_SELECTOR' && process.env.NODE_ENV !== 'production') {
          throw new Error('Component selectors can only be used in conjunction with babel-plugin-emotion.');
        }

        string += key + "{" + handleInterpolation.call(this, obj[key], false) + "}";
      }
    }

    if (Array.isArray(obj)) {
      obj.forEach(_ref, this);
    } else {
      Object.keys(obj).forEach(_ref2, this);
    }

    objectToStringCache.set(obj, string);
    return string;
  }

  var name;
  var labelPattern = /label:\s*([^\s;\n{]+)\s*;/g;

  var createStyles = function createStyles(strings) {
    var stringMode = true;
    var styles = '';
    var identifierName = '';

    if (strings == null || strings.raw === undefined) {
      stringMode = false;
      styles += handleInterpolation.call(this, strings, false);
    } else {
      styles += strings[0];
    }

    for (var _len = arguments.length, interpolations = new Array(_len > 1 ? _len - 1 : 0), _key = 1; _key < _len; _key++) {
      interpolations[_key - 1] = arguments[_key];
    }

    interpolations.forEach(function (interpolation, i) {
      styles += handleInterpolation.call(this, interpolation, styles.charCodeAt(styles.length - 1) === 46 // .
      );

      if (stringMode === true && strings[i + 1] !== undefined) {
        styles += strings[i + 1];
      }
    }, this);
    styles = styles.replace(labelPattern, function (match, p1) {
      identifierName += "-" + p1;
      return '';
    });
    name = emotionUtils.hashString(styles + identifierName) + identifierName;
    return styles;
  };

  if (process.env.NODE_ENV !== 'production') {
    var sourceMapRegEx = /\/\*#\ssourceMappingURL=data:application\/json;\S+\s+\*\//;
    var oldStylis = stylis;

    stylis = function stylis(selector, styles) {
      var result = sourceMapRegEx.exec(styles);
      currentSourceMap = result ? result[0] : '';
      oldStylis(selector, styles);
      currentSourceMap = '';
    };
  }

  function insert(scope, styles) {
    if (caches.inserted[name] === undefined) {
      current = '';
      stylis(scope, styles);
      caches.inserted[name] = current;
    }
  }

  var css = function css() {
    var styles = createStyles.apply(this, arguments);
    var selector = key + "-" + name;

    if (caches.registered[selector] === undefined) {
      caches.registered[selector] = styles;
    }

    insert("." + selector, styles);
    return selector;
  };

  var keyframes = function keyframes() {
    var styles = createStyles.apply(this, arguments);
    var animation = "animation-" + name;
    insert('', "@keyframes " + animation + "{" + styles + "}");
    return animation;
  };

  var injectGlobal = function injectGlobal() {
    var styles = createStyles.apply(this, arguments);
    insert('', styles);
  };

  function getRegisteredStyles(registeredStyles, classNames) {
    var rawClassName = '';
    classNames.split(' ').forEach(function (className) {
      if (caches.registered[className] !== undefined) {
        registeredStyles.push(className);
      } else {
        rawClassName += className + " ";
      }
    });
    return rawClassName;
  }

  function merge(className, sourceMap) {
    var registeredStyles = [];
    var rawClassName = getRegisteredStyles(registeredStyles, className);

    if (registeredStyles.length < 2) {
      return className;
    }

    return rawClassName + css(registeredStyles, sourceMap);
  }

  function cx() {
    for (var _len2 = arguments.length, classNames = new Array(_len2), _key2 = 0; _key2 < _len2; _key2++) {
      classNames[_key2] = arguments[_key2];
    }

    return merge(classnames(classNames));
  }

  function hydrateSingleId(id) {
    caches.inserted[id] = true;
  }

  function hydrate(ids) {
    ids.forEach(hydrateSingleId);
  }

  function flush() {
    if (isBrowser) {
      sheet.flush();
      sheet.inject();
    }

    caches.inserted = {};
    caches.registered = {};
  }

  function _ref3(node) {
    // $FlowFixMe
    sheet.tags[0].parentNode.insertBefore(node, sheet.tags[0]); // $FlowFixMe

    node.getAttribute("data-emotion-" + key).split(' ').forEach(hydrateSingleId);
  }

  if (isBrowser) {
    var chunks = document.querySelectorAll("[data-emotion-" + key + "]");
    Array.prototype.forEach.call(chunks, _ref3);
  }

  var emotion = {
    flush: flush,
    hydrate: hydrate,
    cx: cx,
    merge: merge,
    getRegisteredStyles: getRegisteredStyles,
    injectGlobal: injectGlobal,
    keyframes: keyframes,
    css: css,
    sheet: sheet,
    caches: caches
  };
  return emotion;
}

module.exports = createEmotion;


}).call(this,require('_process'))
},{"_process":4,"emotion-utils":2,"stylis-rule-sheet":5}],2:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

/* eslint-disable */
// murmurhash2 via https://github.com/garycourt/murmurhash-js/blob/master/murmurhash2_gc.js
function hashString(str) {
  return murmurhash2_32_gc(str, str.length).toString(36);
}

function murmurhash2_32_gc(str, seed) {
  var l = str.length,
      h = seed ^ l,
      i = 0,
      k;

  while (l >= 4) {
    k = str.charCodeAt(i) & 0xff | (str.charCodeAt(++i) & 0xff) << 8 | (str.charCodeAt(++i) & 0xff) << 16 | (str.charCodeAt(++i) & 0xff) << 24;
    k = (k & 0xffff) * 0x5bd1e995 + (((k >>> 16) * 0x5bd1e995 & 0xffff) << 16);
    k ^= k >>> 24;
    k = (k & 0xffff) * 0x5bd1e995 + (((k >>> 16) * 0x5bd1e995 & 0xffff) << 16);
    h = (h & 0xffff) * 0x5bd1e995 + (((h >>> 16) * 0x5bd1e995 & 0xffff) << 16) ^ k;
    l -= 4;
    ++i;
  }

  switch (l) {
    case 3:
      h ^= (str.charCodeAt(i + 2) & 0xff) << 16;

    case 2:
      h ^= (str.charCodeAt(i + 1) & 0xff) << 8;

    case 1:
      h ^= str.charCodeAt(i) & 0xff;
      h = (h & 0xffff) * 0x5bd1e995 + (((h >>> 16) * 0x5bd1e995 & 0xffff) << 16);
  }

  h ^= h >>> 13;
  h = (h & 0xffff) * 0x5bd1e995 + (((h >>> 16) * 0x5bd1e995 & 0xffff) << 16);
  h ^= h >>> 15;
  return h >>> 0;
}

var pa = function fa(ha) {
  function V(b, c, d, k, l) {
    for (var a = 0, f = 0, n = 0, e = 0, h, q, m, v = 0, A = 0, B = 0, x = 0, C = 0, p = 0, G = 0, r = 0, N = q = 0, L = 0, t = 0, D = d.length, F = D - 1, g = "", u = "", S = "", M = "", H; r < D;) {
      m = d.charCodeAt(r);
      r === F && 0 !== f + e + n + a && (0 !== f && (m = 47 === f ? 10 : 47), e = n = a = 0, D++, F++);

      if (0 === f + e + n + a) {
        if (r === F && (0 < q && (g = g.replace(P, "")), 0 < g.trim().length)) {
          switch (m) {
            case 32:
            case 9:
            case 59:
            case 13:
            case 10:
              break;

            default:
              g += d.charAt(r);
          }

          m = 59;
        }

        if (1 === N) switch (m) {
          case 123:
          case 125:
          case 59:
          case 34:
          case 39:
          case 40:
          case 41:
          case 44:
            N = 0;

          case 9:
          case 13:
          case 10:
          case 32:
            break;

          default:
            for (N = 0, t = r, h = m, r--, m = 59; t < D;) {
              switch (d.charCodeAt(++t)) {
                case 10:
                case 13:
                case 59:
                  r++, m = h;

                case 58:
                case 123:
                  t = D;
              }
            }

        }

        switch (m) {
          case 123:
            g = g.trim();
            h = g.charCodeAt(0);
            x = 1;

            for (t = ++r; r < D;) {
              m = d.charCodeAt(r);

              switch (m) {
                case 123:
                  x++;
                  break;

                case 125:
                  x--;
              }

              if (0 === x) break;
              r++;
            }

            p = d.substring(t, r);
            0 === h && (h = (g = g.replace(qa, "").trim()).charCodeAt(0));

            switch (h) {
              case 64:
                0 < q && (g = g.replace(P, ""));
                q = g.charCodeAt(1);

                switch (q) {
                  case 100:
                  case 109:
                  case 115:
                  case 45:
                    h = c;
                    break;

                  default:
                    h = W;
                }

                p = V(c, h, p, q, l + 1);
                t = p.length;
                0 < X && 0 === t && (t = g.length);
                0 < E && (h = ia(W, g, L), H = O(3, p, h, c, I, y, t, q, l), g = h.join(""), void 0 !== H && 0 === (t = (p = H.trim()).length) && (q = 0, p = ""));
                if (0 < t) switch (q) {
                  case 115:
                    g = g.replace(ra, sa);

                  case 100:
                  case 109:
                  case 45:
                    p = g + "{" + p + "}";
                    break;

                  case 107:
                    g = g.replace(ta, "$1 $2" + (0 < Q ? T : ""));
                    p = g + "{" + p + "}";
                    p = 1 === w || 2 === w && U("@" + p, 3) ? "@-webkit-" + p + "@" + p : "@" + p;
                    break;

                  default:
                    p = g + p, 112 === k && (p = (u += p, ""));
                } else p = "";
                break;

              default:
                p = V(c, ia(c, g, L), p, k, l + 1);
            }

            S += p;
            p = L = q = G = N = C = 0;
            g = "";
            m = d.charCodeAt(++r);
            break;

          case 125:
          case 59:
            g = (0 < q ? g.replace(P, "") : g).trim();
            if (1 < (t = g.length)) switch (0 === G && (h = g.charCodeAt(0), 45 === h || 96 < h && 123 > h) && (t = (g = g.replace(" ", ":")).length), 0 < E && void 0 !== (H = O(1, g, c, b, I, y, u.length, k, l)) && 0 === (t = (g = H.trim()).length) && (g = "\x00\x00"), h = g.charCodeAt(0), q = g.charCodeAt(1), h + q) {
              case 0:
                break;

              case 169:
              case 163:
                M += g + d.charAt(r);
                break;

              default:
                58 !== g.charCodeAt(t - 1) && (u += ja(g, h, q, g.charCodeAt(2)));
            }
            L = q = G = N = C = 0;
            g = "";
            m = d.charCodeAt(++r);
        }
      }

      switch (m) {
        case 13:
        case 10:
          if (0 === f + e + n + a + ka) switch (B) {
            case 41:
            case 39:
            case 34:
            case 64:
            case 126:
            case 62:
            case 42:
            case 43:
            case 47:
            case 45:
            case 58:
            case 44:
            case 59:
            case 123:
            case 125:
              break;

            default:
              0 < G && (N = 1);
          }
          47 === f ? f = 0 : 0 === z + C && (q = 1, g += "\x00");
          0 < E * la && O(0, g, c, b, I, y, u.length, k, l);
          y = 1;
          I++;
          break;

        case 59:
        case 125:
          if (0 === f + e + n + a) {
            y++;
            break;
          }

        default:
          y++;
          h = d.charAt(r);

          switch (m) {
            case 9:
            case 32:
              if (0 === e + a + f) switch (v) {
                case 44:
                case 58:
                case 9:
                case 32:
                  h = "";
                  break;

                default:
                  32 !== m && (h = " ");
              }
              break;

            case 0:
              h = "\\0";
              break;

            case 12:
              h = "\\f";
              break;

            case 11:
              h = "\\v";
              break;

            case 38:
              0 === e + f + a && 0 < z && (q = L = 1, h = "\f" + h);
              break;

            case 108:
              if (0 === e + f + a + J && 0 < G) switch (r - G) {
                case 2:
                  112 === v && 58 === d.charCodeAt(r - 3) && (J = v);

                case 8:
                  111 === A && (J = A);
              }
              break;

            case 58:
              0 === e + f + a && (G = r);
              break;

            case 44:
              0 === f + n + e + a && (q = 1, h += "\r");
              break;

            case 34:
              0 === f && (e = e === m ? 0 : 0 === e ? m : e);
              break;

            case 39:
              0 === f && (e = e === m ? 0 : 0 === e ? m : e);
              break;

            case 91:
              0 === e + f + n && a++;
              break;

            case 93:
              0 === e + f + n && a--;
              break;

            case 41:
              0 === e + f + a && n--;
              break;

            case 40:
              if (0 === e + f + a) {
                if (0 === C) switch (2 * v + 3 * A) {
                  case 533:
                    break;

                  default:
                    x = 0, C = 1;
                }
                n++;
              }

              break;

            case 64:
              0 === f + n + e + a + G + p && (p = 1);
              break;

            case 42:
            case 47:
              if (!(0 < e + a + n)) switch (f) {
                case 0:
                  switch (2 * m + 3 * d.charCodeAt(r + 1)) {
                    case 235:
                      f = 47;
                      break;

                    case 220:
                      t = r, f = 42;
                  }

                  break;

                case 42:
                  47 === m && 42 === v && (33 === d.charCodeAt(t + 2) && (u += d.substring(t, r + 1)), h = "", f = 0);
              }
          }

          if (0 === f) {
            if (0 === z + e + a + p && 107 !== k && 59 !== m) switch (m) {
              case 44:
              case 126:
              case 62:
              case 43:
              case 41:
              case 40:
                if (0 === C) {
                  switch (v) {
                    case 9:
                    case 32:
                    case 10:
                    case 13:
                      h += "\x00";
                      break;

                    default:
                      h = "\x00" + h + (44 === m ? "" : "\x00");
                  }

                  q = 1;
                } else switch (m) {
                  case 40:
                    C = ++x;
                    break;

                  case 41:
                    0 === (C = --x) && (q = 1, h += "\x00");
                }

                break;

              case 9:
              case 32:
                switch (v) {
                  case 0:
                  case 123:
                  case 125:
                  case 59:
                  case 44:
                  case 12:
                  case 9:
                  case 32:
                  case 10:
                  case 13:
                    break;

                  default:
                    0 === C && (q = 1, h += "\x00");
                }

            }
            g += h;
            32 !== m && 9 !== m && (B = m);
          }

      }

      A = v;
      v = m;
      r++;
    }

    t = u.length;
    0 < X && 0 === t && 0 === S.length && 0 === c[0].length === !1 && (109 !== k || 1 === c.length && (0 < z ? K : R) === c[0]) && (t = c.join(",").length + 2);

    if (0 < t) {
      if (0 === z && 107 !== k) {
        d = 0;
        a = c.length;

        for (f = Array(a); d < a; ++d) {
          v = c[d].split(ua);
          A = "";
          B = 0;

          for (D = v.length; B < D; ++B) {
            if (!(0 === (x = (e = v[B]).length) && 1 < D)) {
              r = A.charCodeAt(A.length - 1);
              L = e.charCodeAt(0);
              n = "";
              if (0 !== B) switch (r) {
                case 42:
                case 126:
                case 62:
                case 43:
                case 32:
                case 40:
                  break;

                default:
                  n = " ";
              }

              switch (L) {
                case 38:
                  e = n + K;

                case 126:
                case 62:
                case 43:
                case 32:
                case 41:
                case 40:
                  break;

                case 91:
                  e = n + e + K;
                  break;

                case 58:
                  switch (2 * e.charCodeAt(1) + 3 * e.charCodeAt(2)) {
                    case 530:
                      if (0 < Y) {
                        e = n + e.substring(8, x - 1);
                        break;
                      }

                    default:
                      if (1 > B || 1 > v[B - 1].length) e = n + K + e;
                  }

                  break;

                case 44:
                  n = "";

                default:
                  e = 1 < x && 0 < e.indexOf(":") ? n + e.replace(va, "$1" + K + "$2") : n + e + K;
              }

              A += e;
            }
          }

          f[d] = A.replace(P, "").trim();
        }

        c = f;
      }

      h = c;
      if (0 < E && (H = O(2, u, h, b, I, y, t, k, l), void 0 !== H && 0 === (u = H).length)) return M + u + S;
      u = h.join(",") + "{" + u + "}";

      if (0 !== w * J) {
        2 !== w || U(u, 2) || (J = 0);

        switch (J) {
          case 111:
            u = u.replace(wa, ":-moz-$1") + u;
            break;

          case 112:
            u = u.replace(Z, "::-webkit-input-$1") + u.replace(Z, "::-moz-$1") + u.replace(Z, ":-ms-input-$1") + u;
        }

        J = 0;
      }
    }

    return M + u + S;
  }

  function ia(b, c, d) {
    var k = c.trim().split(xa);
    c = k;
    var l = k.length,
        a = b.length;

    switch (a) {
      case 0:
      case 1:
        var f = 0;

        for (b = 0 === a ? "" : b[0] + " "; f < l; ++f) {
          c[f] = ma(b, c[f], d, a).trim();
        }

        break;

      default:
        var n = f = 0;

        for (c = []; f < l; ++f) {
          for (var e = 0; e < a; ++e) {
            c[n++] = ma(b[e] + " ", k[f], d, a).trim();
          }
        }

    }

    return c;
  }

  function ma(b, c, d, k) {
    var l = c.charCodeAt(0);
    33 > l && (l = (c = c.trim()).charCodeAt(0));

    switch (l) {
      case 38:
        switch (z + k) {
          case 0:
          case 1:
            if (0 === b.trim().length) break;

          default:
            return c.replace(M, "$1" + b.trim());
        }

        break;

      case 58:
        switch (c.charCodeAt(1)) {
          case 103:
            if (0 < Y && 0 < z) return c.replace(ya, "$1").replace(M, "$1" + R);
            break;

          default:
            return b.trim() + c;
        }

      default:
        if (0 < d * z && 0 < c.indexOf("\f")) return c.replace(M, (58 === b.charCodeAt(0) ? "" : "$1") + b.trim());
    }

    return b + c;
  }

  function ja(b, c, d, k) {
    var l = 0,
        a = b + ";";
    c = 2 * c + 3 * d + 4 * k;

    if (944 === c) {
      l = a.length;
      b = a.indexOf(":", 9) + 1;
      d = a.substring(0, b).trim();
      k = a.substring(b, l - 1).trim();

      switch (a.charCodeAt(9) * Q) {
        case 0:
          break;

        case 45:
          if (110 !== a.charCodeAt(10)) break;

        default:
          for (a = k.split((k = "", za)), b = c = 0, l = a.length; c < l; b = 0, ++c) {
            for (var f = a[c], n = f.split(Aa); f = n[b];) {
              var e = f.charCodeAt(0);
              if (1 === Q && (64 < e && 90 > e || 96 < e && 123 > e || 95 === e || 45 === e && 45 !== f.charCodeAt(1))) switch (isNaN(parseFloat(f)) + (-1 !== f.indexOf("("))) {
                case 1:
                  switch (f) {
                    case "infinite":
                    case "alternate":
                    case "backwards":
                    case "running":
                    case "normal":
                    case "forwards":
                    case "both":
                    case "none":
                    case "linear":
                    case "ease":
                    case "ease-in":
                    case "ease-out":
                    case "ease-in-out":
                    case "paused":
                    case "reverse":
                    case "alternate-reverse":
                    case "inherit":
                    case "initial":
                    case "unset":
                    case "step-start":
                    case "step-end":
                      break;

                    default:
                      f += T;
                  }

              }
              n[b++] = f;
            }

            k += (0 === c ? "" : ",") + n.join(" ");
          }

      }

      k = d + k + ";";
      return 1 === w || 2 === w && U(k, 1) ? "-webkit-" + k + k : k;
    }

    if (0 === w || 2 === w && !U(a, 1)) return a;

    switch (c) {
      case 1015:
        return 45 === a.charCodeAt(9) ? "-webkit-" + a + a : a;

      case 951:
        return 116 === a.charCodeAt(3) ? "-webkit-" + a + a : a;

      case 963:
        return 110 === a.charCodeAt(5) ? "-webkit-" + a + a : a;

      case 1009:
        if (100 !== a.charCodeAt(4)) break;

      case 969:
      case 942:
        return "-webkit-" + a + a;

      case 978:
        return "-webkit-" + a + "-moz-" + a + a;

      case 1019:
      case 983:
        return "-webkit-" + a + "-moz-" + a + "-ms-" + a + a;

      case 883:
        return 45 === a.charCodeAt(8) ? "-webkit-" + a + a : a;

      case 932:
        if (45 === a.charCodeAt(4)) switch (a.charCodeAt(5)) {
          case 103:
            return "-webkit-box-" + a.replace("-grow", "") + "-webkit-" + a + "-ms-" + a.replace("grow", "positive") + a;

          case 115:
            return "-webkit-" + a + "-ms-" + a.replace("shrink", "negative") + a;

          case 98:
            return "-webkit-" + a + "-ms-" + a.replace("basis", "preferred-size") + a;
        }
        return "-webkit-" + a + "-ms-" + a + a;

      case 964:
        return "-webkit-" + a + "-ms-flex-" + a + a;

      case 1023:
        if (99 !== a.charCodeAt(8)) break;
        b = a.substring(a.indexOf(":", 15)).replace("flex-", "").replace("space-between", "justify");
        return "-webkit-box-pack" + b + "-webkit-" + a + "-ms-flex-pack" + b + a;

      case 1005:
        return Ba.test(a) ? a.replace(na, ":-webkit-") + a.replace(na, ":-moz-") + a : a;

      case 1E3:
        b = a.substring(13).trim();
        l = b.indexOf("-") + 1;

        switch (b.charCodeAt(0) + b.charCodeAt(l)) {
          case 226:
            b = a.replace(aa, "tb");
            break;

          case 232:
            b = a.replace(aa, "tb-rl");
            break;

          case 220:
            b = a.replace(aa, "lr");
            break;

          default:
            return a;
        }

        return "-webkit-" + a + "-ms-" + b + a;

      case 1017:
        if (-1 === a.indexOf("sticky", 9)) break;

      case 975:
        l = (a = b).length - 10;
        b = (33 === a.charCodeAt(l) ? a.substring(0, l) : a).substring(b.indexOf(":", 7) + 1).trim();

        switch (c = b.charCodeAt(0) + (b.charCodeAt(7) | 0)) {
          case 203:
            if (111 > b.charCodeAt(8)) break;

          case 115:
            a = a.replace(b, "-webkit-" + b) + ";" + a;
            break;

          case 207:
          case 102:
            a = a.replace(b, "-webkit-" + (102 < c ? "inline-" : "") + "box") + ";" + a.replace(b, "-webkit-" + b) + ";" + a.replace(b, "-ms-" + b + "box") + ";" + a;
        }

        return a + ";";

      case 938:
        if (45 === a.charCodeAt(5)) switch (a.charCodeAt(6)) {
          case 105:
            return b = a.replace("-items", ""), "-webkit-" + a + "-webkit-box-" + b + "-ms-flex-" + b + a;

          case 115:
            return "-webkit-" + a + "-ms-flex-item-" + a.replace(Ca, "") + a;

          default:
            return "-webkit-" + a + "-ms-flex-line-pack" + a.replace("align-content", "") + a;
        }
        break;

      case 953:
        if (0 < (l = a.indexOf("-content", 9)) && 109 === a.charCodeAt(l - 3) && 45 !== a.charCodeAt(l - 4)) return b = a.substring(l - 3), "width:-webkit-" + b + "width:-moz-" + b + "width:" + b;
        break;

      case 962:
        if (a = "-webkit-" + a + (102 === a.charCodeAt(5) ? "-ms-" + a : "") + a, 211 === d + k && 105 === a.charCodeAt(13) && 0 < a.indexOf("transform", 10)) return a.substring(0, a.indexOf(";", 27) + 1).replace(Da, "$1-webkit-$2") + a;
    }

    return a;
  }

  function U(b, c) {
    var d = b.indexOf(1 === c ? ":" : "{"),
        k = b.substring(0, 3 !== c ? d : 10);
    d = b.substring(d + 1, b.length - 1);
    return ba(2 !== c ? k : k.replace(Ea, "$1"), d, c);
  }

  function sa(b, c) {
    var d = ja(c, c.charCodeAt(0), c.charCodeAt(1), c.charCodeAt(2));
    return d !== c + ";" ? d.replace(Fa, " or ($1)").substring(4) : "(" + c + ")";
  }

  function O(b, c, d, k, l, a, f, n, e) {
    for (var h = 0, q = c, m; h < E; ++h) {
      switch (m = ca[h].call(F, b, q, d, k, l, a, f, n, e)) {
        case void 0:
        case !1:
        case !0:
        case null:
          break;

        default:
          q = m;
      }
    }

    switch (q) {
      case void 0:
      case !1:
      case !0:
      case null:
      case c:
        break;

      default:
        return q;
    }
  }

  function da(b) {
    switch (b) {
      case void 0:
      case null:
        E = ca.length = 0;
        break;

      default:
        switch (b.constructor) {
          case Array:
            for (var c = 0, d = b.length; c < d; ++c) {
              da(b[c]);
            }

            break;

          case Function:
            ca[E++] = b;
            break;

          case Boolean:
            la = !!b | 0;
        }

    }

    return da;
  }

  function ea(b) {
    for (var c in b) {
      var d = b[c];

      switch (c) {
        case "keyframe":
          Q = d | 0;
          break;

        case "global":
          Y = d | 0;
          break;

        case "cascade":
          z = d | 0;
          break;

        case "compress":
          oa = d | 0;
          break;

        case "semicolon":
          ka = d | 0;
          break;

        case "preserve":
          X = d | 0;
          break;

        case "prefix":
          ba = null, d ? "function" !== typeof d ? w = 1 : (w = 2, ba = d) : w = 0;
      }
    }

    return ea;
  }

  function F(b, c) {
    if (void 0 !== this && this.constructor === F) return fa(b);
    var d = b,
        k = d.charCodeAt(0);
    33 > k && (k = (d = d.trim()).charCodeAt(0));
    0 < Q && (T = d.replace(Ga, 91 === k ? "" : "-"));
    k = 1;
    1 === z ? R = d : K = d;
    d = [R];

    if (0 < E) {
      var l = O(-1, c, d, d, I, y, 0, 0, 0);
      void 0 !== l && "string" === typeof l && (c = l);
    }

    var a = V(W, d, c, 0, 0);
    0 < E && (l = O(-2, a, d, d, I, y, a.length, 0, 0), void 0 !== l && "string" !== typeof (a = l) && (k = 0));
    K = R = T = "";
    J = 0;
    y = I = 1;
    return 0 === oa * k ? a : a.replace(P, "").replace(Ha, "").replace(Ia, "$1").replace(Ja, "$1").replace(Ka, " ");
  }

  var qa = /^\0+/g,
      P = /[\0\r\f]/g,
      na = /: */g,
      Ba = /zoo|gra/,
      Da = /([,: ])(transform)/g,
      za = /,+\s*(?![^(]*[)])/g,
      Aa = / +\s*(?![^(]*[)])/g,
      ua = / *[\0] */g,
      xa = /,\r+?/g,
      M = /([\t\r\n ])*\f?&/g,
      ya = /:global\(((?:[^\(\)\[\]]*|\[.*\]|\([^\(\)]*\))*)\)/g,
      Ga = /\W+/g,
      ta = /@(k\w+)\s*(\S*)\s*/,
      Z = /::(place)/g,
      wa = /:(read-only)/g,
      Ha = /\s+(?=[{\];=:>])/g,
      Ia = /([[}=:>])\s+/g,
      Ja = /(\{[^{]+?);(?=\})/g,
      Ka = /\s{2,}/g,
      va = /([^\(])(:+) */g,
      aa = /[svh]\w+-[tblr]{2}/,
      ra = /\(\s*(.*)\s*\)/g,
      Fa = /([^]*?);/g,
      Ca = /-self|flex-/g,
      Ea = /[^]*?(:[rp][el]a[\w-]+)[^]*/,
      y = 1,
      I = 1,
      J = 0,
      z = 1,
      w = 1,
      Y = 1,
      oa = 0,
      ka = 0,
      X = 0,
      W = [],
      ca = [],
      E = 0,
      ba = null,
      la = 0,
      Q = 1,
      T = "",
      K = "",
      R = "";
  F.use = da;
  F.set = ea;
  void 0 !== ha && ea(ha);
  return F;
};

var memoize = function memoize(fn) {
  var cache = {};
  return function (arg) {
    if (cache[arg] === undefined) cache[arg] = fn(arg);
    return cache[arg];
  };
};
var STYLES_KEY = '__emotion_styles';
var unitless = {
  animationIterationCount: 1,
  borderImageOutset: 1,
  borderImageSlice: 1,
  borderImageWidth: 1,
  boxFlex: 1,
  boxFlexGroup: 1,
  boxOrdinalGroup: 1,
  columnCount: 1,
  columns: 1,
  flex: 1,
  flexGrow: 1,
  flexPositive: 1,
  flexShrink: 1,
  flexNegative: 1,
  flexOrder: 1,
  gridRow: 1,
  gridRowEnd: 1,
  gridRowSpan: 1,
  gridRowStart: 1,
  gridColumn: 1,
  gridColumnEnd: 1,
  gridColumnSpan: 1,
  gridColumnStart: 1,
  fontWeight: 1,
  lineHeight: 1,
  opacity: 1,
  order: 1,
  orphans: 1,
  tabSize: 1,
  widows: 1,
  zIndex: 1,
  zoom: 1,
  WebkitLineClamp: 1,
  // SVG-related properties
  fillOpacity: 1,
  floodOpacity: 1,
  stopOpacity: 1,
  strokeDasharray: 1,
  strokeDashoffset: 1,
  strokeMiterlimit: 1,
  strokeOpacity: 1,
  strokeWidth: 1
};

exports.memoize = memoize;
exports.STYLES_KEY = STYLES_KEY;
exports.unitless = unitless;
exports.hashString = hashString;
exports.Stylis = pa;


},{}],3:[function(require,module,exports){
(function (global){
'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

function _interopDefault (ex) { return (ex && (typeof ex === 'object') && 'default' in ex) ? ex['default'] : ex; }

var createEmotion = _interopDefault(require('create-emotion'));

var context = typeof global !== 'undefined' ? global : {};

var _createEmotion = createEmotion(context);
var flush = _createEmotion.flush;
var hydrate = _createEmotion.hydrate;
var cx = _createEmotion.cx;
var merge = _createEmotion.merge;
var getRegisteredStyles = _createEmotion.getRegisteredStyles;
var injectGlobal = _createEmotion.injectGlobal;
var keyframes = _createEmotion.keyframes;
var css = _createEmotion.css;
var sheet = _createEmotion.sheet;
var caches = _createEmotion.caches;

exports.flush = flush;
exports.hydrate = hydrate;
exports.cx = cx;
exports.merge = merge;
exports.getRegisteredStyles = getRegisteredStyles;
exports.injectGlobal = injectGlobal;
exports.keyframes = keyframes;
exports.css = css;
exports.sheet = sheet;
exports.caches = caches;


}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})
},{"create-emotion":1}],4:[function(require,module,exports){
// shim for using process in browser
var process = module.exports = {};

// cached from whatever global is present so that test runners that stub it
// don't break things.  But we need to wrap it in a try catch in case it is
// wrapped in strict mode code which doesn't define any globals.  It's inside a
// function because try/catches deoptimize in certain engines.

var cachedSetTimeout;
var cachedClearTimeout;

function defaultSetTimout() {
    throw new Error('setTimeout has not been defined');
}
function defaultClearTimeout () {
    throw new Error('clearTimeout has not been defined');
}
(function () {
    try {
        if (typeof setTimeout === 'function') {
            cachedSetTimeout = setTimeout;
        } else {
            cachedSetTimeout = defaultSetTimout;
        }
    } catch (e) {
        cachedSetTimeout = defaultSetTimout;
    }
    try {
        if (typeof clearTimeout === 'function') {
            cachedClearTimeout = clearTimeout;
        } else {
            cachedClearTimeout = defaultClearTimeout;
        }
    } catch (e) {
        cachedClearTimeout = defaultClearTimeout;
    }
} ())
function runTimeout(fun) {
    if (cachedSetTimeout === setTimeout) {
        //normal enviroments in sane situations
        return setTimeout(fun, 0);
    }
    // if setTimeout wasn't available but was latter defined
    if ((cachedSetTimeout === defaultSetTimout || !cachedSetTimeout) && setTimeout) {
        cachedSetTimeout = setTimeout;
        return setTimeout(fun, 0);
    }
    try {
        // when when somebody has screwed with setTimeout but no I.E. maddness
        return cachedSetTimeout(fun, 0);
    } catch(e){
        try {
            // When we are in I.E. but the script has been evaled so I.E. doesn't trust the global object when called normally
            return cachedSetTimeout.call(null, fun, 0);
        } catch(e){
            // same as above but when it's a version of I.E. that must have the global object for 'this', hopfully our context correct otherwise it will throw a global error
            return cachedSetTimeout.call(this, fun, 0);
        }
    }


}
function runClearTimeout(marker) {
    if (cachedClearTimeout === clearTimeout) {
        //normal enviroments in sane situations
        return clearTimeout(marker);
    }
    // if clearTimeout wasn't available but was latter defined
    if ((cachedClearTimeout === defaultClearTimeout || !cachedClearTimeout) && clearTimeout) {
        cachedClearTimeout = clearTimeout;
        return clearTimeout(marker);
    }
    try {
        // when when somebody has screwed with setTimeout but no I.E. maddness
        return cachedClearTimeout(marker);
    } catch (e){
        try {
            // When we are in I.E. but the script has been evaled so I.E. doesn't  trust the global object when called normally
            return cachedClearTimeout.call(null, marker);
        } catch (e){
            // same as above but when it's a version of I.E. that must have the global object for 'this', hopfully our context correct otherwise it will throw a global error.
            // Some versions of I.E. have different rules for clearTimeout vs setTimeout
            return cachedClearTimeout.call(this, marker);
        }
    }



}
var queue = [];
var draining = false;
var currentQueue;
var queueIndex = -1;

function cleanUpNextTick() {
    if (!draining || !currentQueue) {
        return;
    }
    draining = false;
    if (currentQueue.length) {
        queue = currentQueue.concat(queue);
    } else {
        queueIndex = -1;
    }
    if (queue.length) {
        drainQueue();
    }
}

function drainQueue() {
    if (draining) {
        return;
    }
    var timeout = runTimeout(cleanUpNextTick);
    draining = true;

    var len = queue.length;
    while(len) {
        currentQueue = queue;
        queue = [];
        while (++queueIndex < len) {
            if (currentQueue) {
                currentQueue[queueIndex].run();
            }
        }
        queueIndex = -1;
        len = queue.length;
    }
    currentQueue = null;
    draining = false;
    runClearTimeout(timeout);
}

process.nextTick = function (fun) {
    var args = new Array(arguments.length - 1);
    if (arguments.length > 1) {
        for (var i = 1; i < arguments.length; i++) {
            args[i - 1] = arguments[i];
        }
    }
    queue.push(new Item(fun, args));
    if (queue.length === 1 && !draining) {
        runTimeout(drainQueue);
    }
};

// v8 likes predictible objects
function Item(fun, array) {
    this.fun = fun;
    this.array = array;
}
Item.prototype.run = function () {
    this.fun.apply(null, this.array);
};
process.title = 'browser';
process.browser = true;
process.env = {};
process.argv = [];
process.version = ''; // empty string to avoid regexp issues
process.versions = {};

function noop() {}

process.on = noop;
process.addListener = noop;
process.once = noop;
process.off = noop;
process.removeListener = noop;
process.removeAllListeners = noop;
process.emit = noop;
process.prependListener = noop;
process.prependOnceListener = noop;

process.listeners = function (name) { return [] }

process.binding = function (name) {
    throw new Error('process.binding is not supported');
};

process.cwd = function () { return '/' };
process.chdir = function (dir) {
    throw new Error('process.chdir is not supported');
};
process.umask = function() { return 0; };

},{}],5:[function(require,module,exports){
(function (factory) {
	typeof exports === 'object' && typeof module !== 'undefined' ? (module['exports'] = factory()) :
		typeof define === 'function' && define['amd'] ? define(factory()) :
			(window['stylisRuleSheet'] = factory())
}(function () {

	'use strict'

	return function (insertRule) {
		var delimiter = '/*|*/'
		var needle = delimiter+'}'

		function toSheet (block) {
			if (block)
				try {
					insertRule(block + '}')
				} catch (e) {}
		}

		return function ruleSheet (context, content, selectors, parents, line, column, length, at, depth) {
			switch (context) {
				// property
				case 1:
					// @import
					if (depth === 0 && content.charCodeAt(0) === 64)
						insertRule(content)
					break
				// selector
				case 2:
					if (at === 0)
						return content + delimiter
					break
				// at-rule
				case 3:
					switch (at) {
						// @font-face, @page
						case 102:
						case 112:
							return insertRule(selectors[0]+content), ''
						default:
							return content + delimiter
					}
				case -2:
					content.split(needle).forEach(toSheet)
			}
		}
	}
}))

},{}],6:[function(require,module,exports){
/* Magic Mirror
 * Module: Base-Module
 *
 * By Antreem
 */

// You can use Common.js syntax to require npm modules
//const anything = require("anything");

// Here we load the styling library we picked, emotion https://github.com/emotion-js/emotion
const css = require("emotion").css;

(() => {
    "use strict";
    // Using a self invoking function to ensure 'use strict'

    Module.register("Base-Module", {
        // Default module config.
        defaults: {
            //The configuration to use when the user doesn't select anything
            message: "Hello World",
            //It's a JavaScript object so you can add any type here
            array: [1, 2, 3],
            object: {
                int: 3,
                float: 3.14,
                string: "π",
                array: [3, 1, 4]
            },
            color: "#ff6600"
        },

        start: function() {
            Log.info("Starting module: " + this.name);
            this.sendSocketNotification("START");
        },

        socketNotificationReceived(notification, payload) {
            // This triggers only with notifications from the correspondent node_helper
        },

        notificationReceived(notification, payload, sender) {
            // This triggers with any notification received from any other module (this is **not** a socket notification)
        },

        getStyles() {
            // Here you can define an array of styles to load when the module boots up
            const styles = [
                `${this.name}.css`
                //, "index.css", "whatever.css"
            ];

            return styles;
        },

        getDom() {
            // Here you can define your module content, remember to return the node
            const wrapper = document.createElement("div");
            wrapper.innerText = this.config.message;

            // We can also define a style for our module
            // Here's some documentation for Emotion https://emotion.sh/docs/css
            const style = css`
                color: ${this.config.color};
            `;

            wrapper.classList.add(style);

            return wrapper;
        }
    });
})();

},{"emotion":3}]},{},[6]);
