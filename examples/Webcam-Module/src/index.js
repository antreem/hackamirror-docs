/* Magic Mirror
 * Module: Webcam-Module
 *
 * By Antreem
 */

(() => {
    "use strict";

    Module.register("Webcam-Module", {
        // Default module config.
        defaults: {
            width: 640,
            height: 360
        },

        start: function() {
            Log.info("Starting module: " + this.name);
        },

        socketNotificationReceived(notification, payload) {
            if (notification === "GOT_IMAGE") {
                this.drawPicture(payload);
            }
        },

        drawPicture(picture, index) {
            this.image.src = picture;
        },

        notificationReceived(notification, payload, sender) {
            if (notification === "TAKE_PICTURE") {
                // Sending a TAKE_PICTURE notification from any module triggers
                // a socket notification to let the node_helper handle the request
                this.sendSocketNotification("TAKE_PICTURE");
            }
        },

        getStyles() {
            const styles = [`${this.name}.css`];

            return styles;
        },

        getDom() {
            const width = this.config.width,
                height = this.config.height;

            const wrapper = document.createElement("div"),
                canvas = document.createElement("canvas"),
                image = new Image();

            canvas.width = width;
            canvas.height = height;

            canvas.addEventListener("click", () => {
                // example trigger
                this.sendSocketNotification("TAKE_PICTURE");
            });

            wrapper.appendChild(canvas);

            image.addEventListener("load", () => {
                const ctx = canvas.getContext("2d");
                ctx.clearRect(0, 0, width, height);
                ctx.drawImage(image, 0, 0, width, height);
            });

            this.image = image;

            return wrapper;
        }
    });
})();
