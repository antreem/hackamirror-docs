/* Magic Mirror
 * Module: Base-Module
 *
 * By Antreem
 */

const NodeHelper = require("node_helper"),
    NodeWebcam = require("node-webcam");

Module = {
    register: function(name, moduleDefinition) {
        console.log("Module config loaded: " + name);
        Module.configDefaults[name] = moduleDefinition.defaults;
    }
};

module.exports = NodeHelper.create({
    start: function() {
        console.log("Starting node helper for: " + this.name);
    },

    socketNotificationReceived(notification, payload) {
        console.log("Received notification", notification);

        if (notification === "TAKE_PICTURE") {
            const getImage = () => {
                if (process.platform === "darwin") {
                    NodeWebcam.capture(
                        "test_picture",
                        {
                            callbackReturn: "base64",
                            saveShots: false,
                            delay: 1
                        },
                        (err, data) => {
                            sendImage(data);
                        }
                    );
                } else {
                    const query = `fswebcam --no-banner -r '1280x720' --rotate 0 - | base64`;

                    const out = exec(
                        query,
                        { maxBuffer: 1024 * 1024 },
                        (error, stdout, stderr) => {
                            if (error) {
                                console.error(`exec error: ${error}`);
                                return;
                            }

                            sendImage(`data:image/jpeg;base64,${stdout}`);
                        },
                        {
                            maxbuffer: 1024
                        }
                    );
                }
            };

            const sendImage = image => {
                this.sendSocketNotification("GOT_IMAGE", image);
            };

            getImage();
        }
    }
});
