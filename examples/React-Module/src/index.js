/* Magic Mirror
 * Module: React-Module
 *
 * By Antreem
 */

import React from "react";
import { render } from "react-dom";

(() => {
    class Button extends React.Component {
        render() {
            return <button>{this.props.text}</button>;
        }
    }

    ("use strict");
    Module.register("React-Module", {
        defaults: {},

        start: function() {
            Log.info("Starting module: " + this.name);
        },

        getDom() {
            // Here you can define your module content, remember to return the node
            const root = document.createElement("div");
            root.id = "react-root";

            render(<Button text="Hello, World!" />, root);

            return root;
        }
    });
})();
