const pkg = require("./package.json");

const gulp = require("gulp"),
    browserify = require("browserify"),
    source = require("vinyl-source-stream"),
    buffer = require("vinyl-buffer");

function compile() {
    const bundler = browserify("src/index.js");

    return bundler
        .transform("babelify", { presets: ["es2015", "react"] })
        .bundle()
        .pipe(source(`${pkg.name}.js`))
        .pipe(buffer())
        .pipe(gulp.dest("./"));
}

gulp.task("build:js", function() {
    return compile();
});

gulp.task("build", ["build:js"]);
